package br.com.lais.desafio_android.modelo;

/**
 * Created by Lais on 09/09/2016.
 */
public class JsonRepositoryResponse {
    Repositorio[] items;
    Integer total_count;

    public Repositorio[] getItems() {
        return items;
    }

    public Integer getTotal_count() {
        return total_count;
    }
}
