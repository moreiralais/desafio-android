package br.com.lais.desafio_android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.arasthel.asyncjob.AsyncJob;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.lais.desafio_android.R;
import br.com.lais.desafio_android.adapter.RepositorioAdapter;
import br.com.lais.desafio_android.listener.RecyclerViewOnClickListener;
import br.com.lais.desafio_android.modelo.JsonRepositoryResponse;
import br.com.lais.desafio_android.modelo.Repositorio;
import br.com.lais.desafio_android.servico.GithubAPI;
import br.com.lais.desafio_android.servico.GithubAPIInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrincipalActivity extends AppCompatActivity implements RecyclerViewOnClickListener {

    private RecyclerView recyclerView;
    private ArrayList<Repositorio> data;
    private ArrayList<Repositorio> dataResponse;
    private RepositorioAdapter repositorioAdapter;
    private LinearLayoutManager layoutManager;
    private Toolbar toolbar;
    private int page;
    private Integer total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        toolbar = (Toolbar) findViewById(R.id.principal_toolbar).
                findViewById(R.id.float_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.titulo_principal));
        

        layoutManager = new LinearLayoutManager(PrincipalActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_principal);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        page = 1;
        data = new ArrayList<>();
        loadJSON(page);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
                    RepositorioAdapter repositorioAdapter = (RepositorioAdapter) recyclerView.getAdapter();

                    if (data.size() < total && data.size() == llm.findLastCompletelyVisibleItemPosition() + 1) {

                        Log.i("DATA SIZE == ",""+data.size()+" TOTAL =="+total);

                        page +=1;

                        loadJSON(page);

                        for(int i=0;i<dataResponse.size();i++){
                            repositorioAdapter.addListItem(dataResponse.get(i),data.size());
                        }


                    }
                }
        });

        if (savedInstanceState != null) {
            data = (ArrayList<Repositorio>) savedInstanceState.getSerializable("data");
        }
    }

    private void loadJSON(final int page) {
        GithubAPI githubAPI = GithubAPIInstance.getGithubAPI();

        Log.i("PAGE == ", String.valueOf(page));

        Call<JsonRepositoryResponse> call = githubAPI.getRepositorios(page);

        call.enqueue(new Callback<JsonRepositoryResponse>() {
            @Override
            public void onResponse(Call<JsonRepositoryResponse> call, Response<JsonRepositoryResponse> response) {

                JsonRepositoryResponse jsonResponse = response.body();

                dataResponse = new ArrayList<>(Arrays.asList(jsonResponse.getItems()));

                total = jsonResponse.getTotal_count();

                if(page == 1){
                    data = new ArrayList<>(dataResponse);
                    repositorioAdapter = new RepositorioAdapter(getApplicationContext(),data);
                    repositorioAdapter.setRecyclerViewOnClickListener(PrincipalActivity.this);
                    recyclerView.setAdapter(repositorioAdapter);
                }
            }

            @Override
            public void onFailure(Call<JsonRepositoryResponse> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("data", data);
    }

    @Override
    public void onClickListener(View view, int posicao) {
        int itemPosition = recyclerView.getChildLayoutPosition(view);
        Repositorio item = data.get(itemPosition);

        Toast.makeText(PrincipalActivity.this,"Selecionado: "+item.getOwner().getLogin(),Toast.LENGTH_LONG).show();

        Intent intent = new Intent(PrincipalActivity.this, PullRequestsActivity.class);
        intent.putExtra("repositorio", item);
        startActivity(intent);
    }
}
