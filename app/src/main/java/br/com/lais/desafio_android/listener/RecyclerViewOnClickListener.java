package br.com.lais.desafio_android.listener;

import android.view.View;

/**
 * Created by Lais on 10/09/2016.
 */
public interface RecyclerViewOnClickListener {
    public void onClickListener(View view,int posicao);
}
