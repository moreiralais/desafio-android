package br.com.lais.desafio_android.servico;

import java.util.List;

import br.com.lais.desafio_android.modelo.JsonRepositoryResponse;
import br.com.lais.desafio_android.modelo.PullRequest;
import br.com.lais.desafio_android.modelo.Repositorio;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Lais on 09/09/2016.
 */
public interface GithubAPI {

    String ENDPOINT = "https://api.github.com";

    @GET("/search/repositories?q=language:Java&sort=stars")
    Call<JsonRepositoryResponse> getRepositorios(@Query("page") int page);

    @GET("/repos/{criador}/{repositorio}/pulls")
    Call<PullRequest[]> getPullRequests(@Path("criador") String criador, @Path("repositorio") String repositorio);
}
