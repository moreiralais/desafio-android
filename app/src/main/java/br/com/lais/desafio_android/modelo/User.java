package br.com.lais.desafio_android.modelo;

/**
 * Created by Lais on 09/09/2016.
 */
public class User {
    String login;
    String avatar_url;

    public String getLogin() {
        return login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }
}
