package br.com.lais.desafio_android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arasthel.asyncjob.AsyncJob;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import br.com.lais.desafio_android.R;
import br.com.lais.desafio_android.listener.RecyclerViewOnClickListener;
import br.com.lais.desafio_android.modelo.Repositorio;

/**
 * Created by Lais on 09/09/2016.
 */
public class RepositorioAdapter extends RecyclerView.Adapter<RepositorioAdapter.ViewHolder> {
    private ArrayList<Repositorio> repositorios;
    private Context contexto;
    private LayoutInflater layoutInflater;
    private RecyclerViewOnClickListener recyclerViewOnClickListener;

    public RepositorioAdapter(Context contexto,ArrayList<Repositorio> repositorios) {
        this.repositorios = repositorios;
        this.contexto = contexto;
        layoutInflater = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RepositorioAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.card_principal, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepositorioAdapter.ViewHolder viewHolder, int i) {

        viewHolder.nome_repositorio.setText(repositorios.get(i).getName());
        viewHolder.descricao_repositorio.setText(repositorios.get(i).getDescription());
        viewHolder.numero_forks.setText(String.valueOf(repositorios.get(i).getForks()));
        viewHolder.numero_stars.setText(String.valueOf(repositorios.get(i).getStargazers_count()));
        viewHolder.nome_autor.setText(repositorios.get(i).getOwner().getLogin());
        Picasso.with(contexto)
                .load(repositorios.get(i).getOwner().getAvatar_url())
                .placeholder(R.mipmap.ic_account)
                .error(R.mipmap.ic_account)
                .into(viewHolder.foto_autor);

    }

    @Override
    public int getItemCount() {
        return repositorios.size();
    }

    public void setRecyclerViewOnClickListener(RecyclerViewOnClickListener r){
        recyclerViewOnClickListener = r;
    }

    public void addListItem(Repositorio r, int posicao) {
                Log.i("ADD ITEM == ","START" + posicao);
                repositorios.add(r);
                notifyItemInserted(posicao);
    }

    public ArrayList<Repositorio> getRepositorios(){
        return repositorios;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView nome_repositorio,descricao_repositorio,numero_forks,numero_stars,nome_autor;
        private ImageView foto_autor;

        public ViewHolder(View view) {
            super(view);

            nome_repositorio = (TextView)view.findViewById(R.id.nome_repositorio);
            descricao_repositorio = (TextView)view.findViewById(R.id.descricao_repositorio);
            numero_forks = (TextView)view.findViewById(R.id.numero_forks);
            numero_stars = (TextView)view.findViewById(R.id.numero_stars);
            nome_autor = (TextView)view.findViewById(R.id.nome_autor);
            foto_autor = (ImageView)view.findViewById(R.id.foto_autor);

            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if(recyclerViewOnClickListener!=null){
                recyclerViewOnClickListener.onClickListener(v,getAdapterPosition());
            }
        }
    }
}
