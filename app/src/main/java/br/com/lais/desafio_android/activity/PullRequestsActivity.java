package br.com.lais.desafio_android.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;

import br.com.lais.desafio_android.R;
import br.com.lais.desafio_android.adapter.PullRequestAdapter;
import br.com.lais.desafio_android.listener.RecyclerViewOnClickListener;
import br.com.lais.desafio_android.modelo.PullRequest;
import br.com.lais.desafio_android.modelo.Repositorio;
import br.com.lais.desafio_android.servico.GithubAPI;
import br.com.lais.desafio_android.servico.GithubAPIInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullRequestsActivity extends AppCompatActivity implements RecyclerViewOnClickListener {

    private RecyclerView recyclerView;
    private ArrayList<PullRequest> data;
    private PullRequestAdapter pullRequestAdapter;
    private LinearLayoutManager layoutManager;
    private Repositorio repositorio;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        toolbar = (Toolbar) findViewById(R.id.pull_toolbar).
                findViewById(R.id.float_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.titulo_pull));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_voltar);

        layoutManager = new LinearLayoutManager(PullRequestsActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_pull);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);


        Repositorio repositorioExtra = (Repositorio) getIntent().getExtras().getSerializable("repositorio");
        if (repositorioExtra != null) {
            repositorio = repositorioExtra;
        }

        loadJSON();


        if (savedInstanceState != null) {
            data = (ArrayList<PullRequest>) savedInstanceState.getSerializable("data");
        }

    }

    private void loadJSON() {
        GithubAPI githubAPI = GithubAPIInstance.getGithubAPI();

        Call<PullRequest[]> call = githubAPI.getPullRequests(repositorio.getOwner().getLogin(),repositorio.getName());

        call.enqueue(new Callback<PullRequest[]>() {
            @Override
            public void onResponse(Call<PullRequest[]> call, Response<PullRequest[]> response) {

                PullRequest[] jsonResponse = response.body();

                data = new ArrayList<>(Arrays.asList(jsonResponse));

                Log.i("Response body === ",data.get(0).getTitle());

                pullRequestAdapter = new PullRequestAdapter(getApplicationContext(),data);
                pullRequestAdapter.setRecyclerViewOnClickListener(PullRequestsActivity.this);
                recyclerView.setAdapter(pullRequestAdapter);
            }

            @Override
            public void onFailure(Call<PullRequest[]> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("data", data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pull, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id)
        {
            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickListener(View view, int posicao) {
        int itemPosition = recyclerView.getChildLayoutPosition(view);
        PullRequest item = data.get(itemPosition);

        String url = item.getHead().getRepo().getHtml_url();
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
