package br.com.lais.desafio_android.modelo;

/**
 * Created by Lais on 09/09/2016.
 */
public class PullRequest {

    User user;
    String title;
    String created_at; //2016-09-08T15:35:13Z
    String body;
    Head head;

    public User getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getBody() {
        return body;
    }

    public Head getHead() {
        return head;
    }
}
