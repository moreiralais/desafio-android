package br.com.lais.desafio_android.servico;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lais on 09/09/2016.
 */
public class GithubAPIInstance {

    public static GithubAPI getGithubAPI() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(GithubAPI.ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit.create(GithubAPI.class);
    }
}
