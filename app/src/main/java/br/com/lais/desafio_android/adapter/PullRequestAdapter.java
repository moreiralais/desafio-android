package br.com.lais.desafio_android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import br.com.lais.desafio_android.R;
import br.com.lais.desafio_android.listener.RecyclerViewOnClickListener;
import br.com.lais.desafio_android.modelo.PullRequest;
import br.com.lais.desafio_android.modelo.Repositorio;

/**
 * Created by Lais on 09/09/2016.
 */
public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder> {
    private ArrayList<PullRequest> pullRequests;
    private Context contexto;
    private LayoutInflater layoutInflater;
    private RecyclerViewOnClickListener recyclerViewOnClickListener;

    public PullRequestAdapter(Context contexto, ArrayList<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
        this.contexto = contexto;
        layoutInflater = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PullRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.card_pull_request, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PullRequestAdapter.ViewHolder viewHolder, int i) {

        viewHolder.nome_pr.setText(pullRequests.get(i).getUser().getLogin());
        viewHolder.titulo_pr.setText(pullRequests.get(i).getTitle());
        viewHolder.data_pr.setText(pullRequests.get(i).getCreated_at());
        viewHolder.body_pr.setText(pullRequests.get(i).getBody());
        Picasso.with(contexto)
                .load(pullRequests.get(i).getUser().getAvatar_url())
                .placeholder(R.mipmap.ic_account)
                .error(R.mipmap.ic_account)
                .into(viewHolder.foto_pr);

    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public void setRecyclerViewOnClickListener(RecyclerViewOnClickListener r){
        recyclerViewOnClickListener = r;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView nome_pr,titulo_pr,data_pr,body_pr;
        private ImageView foto_pr;

        public ViewHolder(View view) {
            super(view);

            nome_pr = (TextView)view.findViewById(R.id.nome_pr);
            titulo_pr = (TextView)view.findViewById(R.id.titulo_pr);
            data_pr = (TextView)view.findViewById(R.id.data_pr);
            body_pr = (TextView)view.findViewById(R.id.body_pr);
            foto_pr = (ImageView)view.findViewById(R.id.foto_pr);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(recyclerViewOnClickListener!=null){
                recyclerViewOnClickListener.onClickListener(v,getAdapterPosition());
            }
        }
    }
}
