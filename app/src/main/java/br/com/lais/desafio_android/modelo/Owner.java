package br.com.lais.desafio_android.modelo;

import java.io.Serializable;

/**
 * Created by Lais on 09/09/2016.
 */
public class Owner implements Serializable{

    String login;
    String avatar_url;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}
